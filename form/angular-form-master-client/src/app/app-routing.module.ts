import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import {PaymentComponent} from "./form/payment/payment.component";


const routes: Routes = [
  {
    // path: 'form',
    // component: FormComponent
    path: 'payment',
    component: PaymentComponent
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }
