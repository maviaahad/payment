import { Injectable } from '@angular/core';
import {environment} from '../../environment/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {tap} from "rxjs/operators";
import * as socketIo from 'socket.io-client';


@Injectable({
  providedIn: 'root'
})


export class FormService {
  public serverURL: string;
  public socket;
  constructor(private http: HttpClient) {
    this.serverURL = environment.serverUrl;
  }


  public initSocket(channelId,phone): void {
    if(!this.socket){
      this.socket = socketIo(environment.socketConfig.url);
      this.socket.emit('connected',{channelId:channelId, number:phone});
    }
  }

  public removeSocket():void{
    if(this.socket){

      this.socket = undefined;
    }
  }


  getCallDisconnectNotification() {

    return new Observable<any>(observer => {
      this.socket.on('disconnection', (data: any) =>{ console.log(data); observer.next(data)});
    });
}

  public createFormData(data) {
    const url = `${this.serverURL}/insurance-form/create`;
    console.log(url)
    return this.http.post(url,data)
      .pipe(
        tap(result=>{  console.log("received");console.log(result)})
      );

  }
  public fetchFormData(data){
  console.log(data)
    const url = `http://bc6d9e223772.ngrok.io/insurance-form/fetch`;
    console.log(this.serverURL)
    return this.http.post(url,data)
      .pipe(
        tap(result=>{console.log(result)})
      );

}

public voiceApi(data){

  console.log("voice api 65")

  const url = `https://ivr.humonics.ai/ivr-visual/`;
  // const url = `http://13.235.175.133:5000`;
  return this.http.post(url,data)
    .pipe(
      tap(result=>{console.log(result)})
    );

}










}
