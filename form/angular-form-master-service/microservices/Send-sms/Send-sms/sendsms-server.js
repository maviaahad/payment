// const config = require('../../common/utils/config-util')
const router = require('../Send-sms/routes/sendsms-route')

const helmet = require('helmet');
const cors = require('cors');
const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const path = require('path');

/**************************** Middleware *******************************************/
app.use(helmet());
app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({limit: '20mb', extended: true}));
app.use(cors());


/**************************** Routes *******************************************/

app.use('/',router)

/*******************************Server Hosting*****************************************/
const server = app.listen(3900, function() {
    console.log('sendSms server is running on port 3900' );
});


